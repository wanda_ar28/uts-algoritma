public class BinarySearch {
    public static void main(String[] args) {
        int[] data = {3, 7, 10, 15, 91, 110, 150};
        int cari = 91;
        int kiri = 0;
        int tengah;
        int kanan = data.length - 1;
        while (kiri <= kanan) {
            tengah = (kiri + kanan)/ 2;
            if (data [tengah] == cari) {
            System.out.println("Elemen ditemukan pada indeks ke-" + tengah);
            break;
            } else if (data[tengah] < cari) {
            kiri = tengah + 1;
            } else if (data[tengah] > cari) {
            kanan = tengah - 1;
            }
        }
    }
}
 