public class ArrayMultiDimensi {
    public static void main(String[] args) {
        String mahasiswa[][] = {
            {
                "021210009", "Anselmus Yoga Pamungkas"
            },
            {
                "021210049", "Selly Meilinda"
            },
            {
                "021210067", "Epa Foditus Luahambowo"
            },
            {
                "021210034", "Alham"
            },
            {
                "021210039", "Riki Ronaldo"
            }
        };
        System.out.println("Nama "+ mahasiswa[0][1] +" "
                + "berada pada baris 1, kolom ke 2");
        System.out.println("NPM "+ mahasiswa[3][0] +" "
                + "berada pada baris 4, kolom ke 1");
    }
}
